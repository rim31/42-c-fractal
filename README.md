# 42-C-fractal
# envionment
mac os x el capitan

mini lib x

langage C

# Description 
This programm draw different fractals : Mandelbrot, Julia, burning ship, bird of prey, etc...

# run :
git clone https://github.com/rim31/42-C-fractal.git

cd 42-C-fractal

make

./fractol 1

(try other fractal with a number between 1 - 8)

# basic render :
![Texte alternatif](https://github.com/rim31/42-C-fractal/blob/master/frac.png "make")

./fractol 1

move with arrow

Esc to quit the prgm


![Texte alternatif](https://github.com/rim31/42-C-fractal/blob/master/frac%201.png "mandelbrot")

# bonus render
![Texte alternatif](https://github.com/rim31/42-C-fractal/blob/master/frac%202.png "zoom")

bonus : zoom and color with mouse :

boutton 3 and mouse wheel


# Ultimate bonus 
multi windows pressing numeric keypad '1' to '8'

'9' for menu

![Texte alternatif](https://bitbucket.org/rim31/42-c-fractal/src/5283434d3de476d0495d97bb2f6a652a1d6b5f7c/frac%20bonus.png?fileviewer=file-view-default "bonus max")